package org.wit.mytweet.activities;

import java.util.UUID;
import org.wit.android.helpers.ContactHelper;
import org.wit.practice.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Content;
import org.wit.mytweet.models.Tweet;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static org.wit.android.helpers.IntentHelper.sendEmail;
import static org.wit.android.helpers.IntentHelper.navigateUp;

// The actual MyTweetFragment class that is used in the app.
public class MyTweetFragment extends Fragment implements TextWatcher, View.OnClickListener, Callback<Tweet>
{
    public static   final String  EXTRA_TWEET_ID = "mytweet.TWEET_ID";

    private static  final int     REQUEST_CONTACT = 1;

    private EditText tweetout;
    private Button   contactButton;
    private Button   emailButton;
    private Button tweetButton;
    private TextView date;
    private TextView charCountAmount;

    private Tweet   tweet;
    private Content   content;

    MyTweetApp app;
    // Items that will be created when the fragment is launched. Loads tweets and list of tweets from Content class.
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        UUID twtId = (UUID)getArguments().getSerializable(EXTRA_TWEET_ID);
        app = (MyTweetApp) getActivity().getApplication();
        content = app.content;
        tweet = content.getTweet(twtId);
        transmitTweet();

    }

    // method to reate a view object named v that enables the actionbar (title & up button)
    // also calls the addListeners & updateControls methods implemented later

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        super.onCreateView(inflater, parent, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_tweet, parent, false);

        getActivity().getActionBar().setTitle(R.string.tweetout);
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        addListeners(v);
        updateControls(tweet);
        return v;
    }

    private void addListeners(View v)
    {
        tweetout = (EditText) v.findViewById(R.id.tweetout);
        contactButton = (Button)  v.findViewById(R.id.contact);
        emailButton = (Button)  v.findViewById(R.id.emailButton);
        charCountAmount = (TextView) v.findViewById(R.id.charCount);
        tweetButton = (Button) v.findViewById(R.id.tweetButton);
        date = (TextView)v.findViewById(R.id.currentDate);


        tweetout .addTextChangedListener(this);
        contactButton.setOnClickListener(this);
        emailButton.setOnClickListener(this);
        tweetButton.setOnClickListener(this);

    }

    public void updateControls(Tweet tweet) {
        charCountAmount.setText(String.valueOf(tweet.charCount));
        tweetout.setText(tweet.tweetout);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home: navigateUp(getActivity());
                return true;
            default:                return super.onOptionsItemSelected(item);
        }
    }

     // a short method to save the tweets to the phone upon closing (pausing) the app.

    @Override
    public void onPause()
    {
        super.onPause();
        content.saveTweets();
    }

    // a method to call the contact from the phone book on the actual phone hosting the app.

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode != Activity.RESULT_OK)
        {
            return;
        }
        else
        if (requestCode == REQUEST_CONTACT)
        {
            String name = ContactHelper.getContact(getActivity(), data);
            tweet.tweetwho = name;
            contactButton.setText(name);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    { }

    //a method that counts down the number of characters on the charCountAmount string
    // Once the characters reach 140 a toast appears to tell the user this.

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        charCountAmount.setText(String.valueOf(tweet.charCount - s.length()));
        charCountAmount.setText(String.valueOf(140 - (tweetout.length())));
        if ((140 -tweetout.length()) < 1)
        {
            charCountAmount.setTextColor(Color.parseColor("#FF0000"));
            Toast toast = Toast.makeText((getActivity()), "Character Limit Reached!", Toast.LENGTH_SHORT);
            toast.show();
        }
        else{

            charCountAmount.setTextColor(Color.parseColor("#FFFFFF"));
        }
    }

    //Method used to log any changed to the text that the user can edit (tweetout)
    @Override
    public void afterTextChanged(Editable c)
    {
        Log.i(this.getClass().getSimpleName(), "tweetout " + c.toString());
        tweet.tweetout = c.toString();
        transmitTweet();
    }

// Various methods to start activities when buttons are pressed.
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
// when tweet button is pressed returns tweet sent unless the maximum chaacters has been reached.
            case R.id.tweetButton:

                if ((140 -tweetout.length()) < 1)
                {

                    Toast toast = Toast.makeText(getActivity(), "Maximum characters reached!",Toast.LENGTH_LONG);
                    toast.show();
                }

                else
                {
                    Toast toast = Toast.makeText(getActivity(), "Tweet Sent!", Toast.LENGTH_SHORT);
                    toast.show();
                }


                break;
// similar buttons for when the user chooses a contact or e-mail from the tweet activity.

            case R.id.contact                 : Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
                if (tweet.tweetwho != null)
                {
                    contactButton.setText("Tweeter: "+ tweet.tweetwho);
                }
                break;
            case R.id.emailButton : sendEmail(getActivity(), "", getString(R.string.tweet_subject), tweet.getTweetReport(getActivity()));
                break;

        }
    }

    public void transmitTweet()
    {
        Call<Tweet> call = app.tweetService.createTweet(tweet.id.toString(), tweet);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Response<Tweet> response, Retrofit retrofit) {
        Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(Throwable t) {
        Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();

    }
}
