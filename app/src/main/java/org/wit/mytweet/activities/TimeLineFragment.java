package org.wit.mytweet.activities;

import java.util.ArrayList;

import org.wit.android.helpers.IntentHelper;
import org.wit.practice.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Content;
import org.wit.mytweet.models.Tweet;

import android.widget.AbsListView;
import android.widget.ListView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.widget.AdapterView.OnItemClickListener;
import android.view.ActionMode;
import android.widget.Toast;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class TimeLineFragment extends ListFragment implements OnItemClickListener, AbsListView.MultiChoiceModeListener, Callback<Tweet>
{
    private ArrayList<Tweet> tweets;
    private Content content;
    private TimeLineAdapter adapter;
    private ListView listView;
    MyTweetApp app;
    private Tweet   tweet;

    // upon creation loads the options menu and sets the title of the activity. Also creates a new TimeLineAdapter
    // adds a new tweet to the list of tweets in content.
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.timelinestring);

        MyTweetApp app = (MyTweetApp) getActivity().getApplication();
        content = app.content;
        tweets = content.tweets;

        adapter = new TimeLineAdapter(getActivity(), tweets);
        setListAdapter(adapter);

    }
// Loads a listview of tweets and allows the user to choose individual tweets
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        View v = super.onCreateView(inflater, parent, savedInstanceState);
        listView = (ListView) v.findViewById(android.R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(this);
        return v;
    }
//method used to find the correct tweet by ID using the adapter.
    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        Tweet twt = ((TimeLineAdapter) getListAdapter()).getItem(position);
        Intent i = new Intent(getActivity(), MyTweetPager.class);
        i.putExtra(MyTweetFragment.EXTRA_TWEET_ID, twt.id);
        startActivityForResult(i, 0);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        ((TimeLineAdapter) getListAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.timelinelist, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_item_new_tweet:
                Tweet tweet = new Tweet();

                // API call after tweet fully formed
                content.addTweet(tweet);

                Intent i = new Intent(getActivity(), MyTweetPager.class);
                i.putExtra(MyTweetFragment.EXTRA_TWEET_ID, tweet.id);
                startActivityForResult(i, 0);
                transmitTweet();
                return true;
// a case to allow for the deletion of all tweets on the timeline page.
            case R.id.clear:
                content.removeAllTweets();
                adapter.notifyDataSetChanged();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Tweet tweet = adapter.getItem(position);
        IntentHelper.startActivityWithData(getActivity(), MyTweetPager.class, "TWEET_ID", tweet.id);
    }

    class TimeLineAdapter extends ArrayAdapter<Tweet>
    {
        private Context context;

        public TimeLineAdapter(Context context, ArrayList<Tweet> tweets)
        {
            super(context, 0, tweets);
            this.context = context;
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null)
            {
                convertView = inflater.inflate(R.layout.list_item_timeline, null);
            }
            Tweet twt = getItem(position);

            TextView tweetout = (TextView) convertView.findViewById(R.id.timeline_list_item_tweetout);
            tweetout.setText(twt.tweetout);

            TextView currentDate = (TextView) convertView.findViewById(R.id.timeline_list_item_dateTextView);
            currentDate.setText(twt.getDateString());

            return convertView;
        }
    }

    /* ************ MultiChoiceModeListener methods (begin) *********** */
    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu)
    {
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.timeline_context, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu)
    {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem)
    {
        switch (menuItem.getItemId())
        {
            case R.id.menu_item_delete_tweet:
                deleteTweet(actionMode);
                return true;
            default:
                return false;
        }

    }
// a method to delete a tweet from the list of tweets in timeline.
    private void deleteTweet(ActionMode actionMode)
    {
        for (int i = adapter.getCount() - 1; i >= 0; i--)
        {
            if (listView.isItemChecked(i))
            {
                content.deleteTweet(adapter.getItem(i));
            }
        }
        actionMode.finish();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode)
    {
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked)
    {
    }
    public void transmitTweet()
    {
        Call<Tweet> call = app.tweetService.createTweet(tweet.id.toString(), tweet);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Response<Tweet> response, Retrofit retrofit) {
        Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(Throwable t) {
        Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();

    }
  /* ************ MultiChoiceModeListener methods (end) *********** */
}