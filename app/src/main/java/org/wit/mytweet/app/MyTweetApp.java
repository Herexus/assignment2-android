package org.wit.mytweet.app;

import android.app.Application;

import org.wit.mytweet.main.TweetServiceProxy;
import org.wit.mytweet.models.Content;
import org.wit.mytweet.models.ContentSerializer;

import static org.wit.android.helpers.LogHelpers.info;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class MyTweetApp extends Application
{

        private static final String FILENAME = "content.json";

        public Content content;

        public String  service_url = "http://10.0.3.2:9000"; //Genymotion

        public TweetServiceProxy tweetService;

        @Override
        public void onCreate()
        {
            super.onCreate();
            ContentSerializer serializer = new ContentSerializer(this, FILENAME);
            content = new Content(serializer);

            info(this, "MyTweet app launched");

            Gson gson = new GsonBuilder().create();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(service_url)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            tweetService = retrofit.create(TweetServiceProxy.class);
        }
}